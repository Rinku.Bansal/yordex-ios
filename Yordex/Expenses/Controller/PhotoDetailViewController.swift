//
//  PhotoViewController.swift
//  Yordex
//
//  Created by vinove on 19/09/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
import ImageScrollView

class PhotoDetailViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var imageScrollView: ImageScrollView!

    
    var expenseImage:UIImage?
    
    override func viewDidLoad() {

        super.viewDidLoad()
        imageScrollView.setup()

        if let img = expenseImage
        {
            imageScrollView.display(image: img)

        }
        imageScrollView.imageScrollViewDelegate = self

        // scrollView.delegate = self - it is set on the storyboard.
    }
    
}
extension PhotoDetailViewController: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}
