//
//  HomeViewController.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 01/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Photos
import SwiftyJSON

class HomeViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    @IBOutlet weak var logoutBarButton: UIBarButtonItem!

    @IBOutlet weak var expenseTableView: UITableView!
    @IBOutlet weak var hud: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableBottomContraint: NSLayoutConstraint!
    var isSearch: Bool = false
    var eventId = ""
    // MARK: - Variables
    var expenseList = [ExpenseModel]()
    var searchList = [ExpenseModel]()
    var pickedImage = [String:Any]()

    
    var page = 0
    var isDataLoading = false
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBarButton(fontSize: 30)
        expenseTableView.tableFooterView = UIView()

        let font = UIFont.systemFont(ofSize: 17)
        logoutBarButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for:.normal)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.searchBar.text = ""
       self.loadExpenses()
        
    }
    func loadExpenses() {
        page = 0
        isSearch = false
        expenseList = []
        configureView()
        searchList.removeAll()
        expenseList.removeAll()
        getExpenseList()
    }
    func configureView() {
        tableBottomContraint.constant = 0
    }
    
    @IBAction func logoutAction(_ sender: Any) {

        let alert = UIAlertController(title: "Alert", message: "Do you want to logout?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    func logout()
    {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        self.showHud()
        NetworkManager.logout(parameters: [:], method: .delete) { (success, json, message) in
            self.hideHud()
            if success {
                UserDefaults.standard.Authorization = ""
                UserDefaults.standard.userId = ""
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let homePage = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                appDelegate.window?.rootViewController = homePage
            }else {
                self.alert(message: message)
            }
        }
    }
    @objc func profileBtnAction()  {
        self.searchBar.resignFirstResponder()
        self.isSearch = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: - Actions
    @IBAction func addBarButtonTapped(_ sender: Any) {
        self.isSearch = false
        self.searchBar.resignFirstResponder()
        if addBarButton.title == "+" {
            navigationItem.title = "Create Expense"
            addBarButton.title = "Close"
            setUpBarButton(fontSize: 17)
            openDropdown()
        } else {
            navigationItem.title = "My Expenses"
            addBarButton.title = "+"
            setUpBarButton(fontSize: 30)
            remove()
        }
    }
    // MARK: - Helper methods
    func setUpBarButton(fontSize: CGFloat) {
        let font = UIFont.systemFont(ofSize: fontSize)
        addBarButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for:.normal)
    }
    func openDropdown() {
        let vc = ExpensePopUpViewController.instantiate(fromAppStoryboard: .Main)
        vc.expenseDelegate = self
        add(vc)
    }
    
    
    
}
// MARK: - ExpenseDelegate
extension HomeViewController: ExpenseDelegate {
    func selected(index: Int) {
        addBarButtonTapped(addBarButton ?? UIBarButtonItem())
        switch index {
        case 0:
            print("index = 0")
            let vc = ManuallyAddExpenseViewController.instantiate(fromAppStoryboard: .Main)
            navigationController?.pushViewController(vc, animated: true)
            break
        case 1:
            let vc = ScanReceiptController.instantiate(fromAppStoryboard: .Main)
            navigationController?.pushViewController(vc, animated: true)
            break
        default:
            print("index = 1")
        }
    }
}
// MARK: - TableView methods
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return searchList.count
        }else {
            return expenseList.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearch {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExpenseTableViewCell.rawValue, for: indexPath) as! ExpenseTableViewCell
            cell.expense = searchList[indexPath.row]
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExpenseTableViewCell.rawValue, for: indexPath) as! ExpenseTableViewCell
            print(expenseList.count)
            if expenseList.count > 0 {
                cell.expense = expenseList[indexPath.row]
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        let vc = DetailsViewController.instantiate(fromAppStoryboard: .Main)
        if isSearch
        {
            vc.expenseModel = searchList[indexPath.row]
        }
        else
        {
            vc.expenseModel = expenseList[indexPath.row]

        }
        navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isSearch {
            if indexPath.row == expenseList.count - 1 {
                page += 1
                tableBottomContraint.constant = 44
                getExpenseList()
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81
    }
    func reload() {
        expenseTableView.reloadData()
        tableBottomContraint.constant = 0
    }
}
// MARK: - Web Services
extension HomeViewController {
    func getExpenseList() {
       
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        
        self.isSearch = false
        if expenseList.count == 0 {
            DispatchQueue.main.async {
                
                self.showHud()
                
            }
        }
            let parameters: Parameters = ["buyerTagValues": "EXPENSE", "returnCompletedOrders": false, "userId": UserDefaults.standard.userId,  "size": 25]
            NetworkManager.getExpenseList(parameters: parameters, method: .get, page: self.page) { (success, json, message) in
                    self.hideHud()
                if success {
                    if let _embedded = json["_embedded"].dictionary, let orders = _embedded["orders"]?.array {
                        orders.forEach({ (data) in
                            let expense = ExpenseModel(data)
                            self.expenseList.append(expense)
                        })
                        DispatchQueue.main.async {
                            self.reload()
                       }
                    }
                } else {
                    self.alert(message: message)
                }
            }
      //  }
    }
}

// MARK: - SearchBarDelegate
extension HomeViewController : UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isSearch = true
        if searchText.count == 0
        {
            self.isSearch = false
            self.searchList.removeAll()
            self.expenseTableView.reloadData()
        }
        self.searchList.removeAll()
        for subIndex in 0...self.expenseList.count-1
        {
            let name = self.expenseList[subIndex].merchantName?.lowercased()
            if name?.contains(searchText.lowercased()) ?? false
            {
                self.searchList.append(self.expenseList[subIndex])
             }
        }
        self.expenseTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if self.searchBar.text?.count ?? 0 == 0 {
            self.isSearch = false
            self.searchList.removeAll()
            self.expenseTableView.reloadData()
        }
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.isSearch = false
        self.searchList.removeAll()
        self.expenseTableView.reloadData()
    }
}

