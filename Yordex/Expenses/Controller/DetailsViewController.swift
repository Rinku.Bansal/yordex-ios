//
//  DetailsViewController.swift
//  Yordex
//
//  Created by vinove on 16/09/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
import MBProgressHUD
import Photos
import SwiftyJSON
import Alamofire

class DetailsViewController: UIViewController {

    @IBOutlet weak var employeeLabel: UILabel!
    @IBOutlet weak var merchantLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var reimbursedAmountLabel: UILabel!
    @IBOutlet weak var expenseAmountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var costAllocationLabel: UILabel!
    @IBOutlet weak var receiptLabel: UILabel!
    @IBOutlet weak var addCostButton: UIButton!
    @IBOutlet weak var addReceiptButton: UIButton!
    var expenseModel:ExpenseModel?
    var pickedImage = [String:Any]()
    var expenseImage:UIImage?

    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Expense Summary"
        if let expense = self.expenseModel
        {
        self.displayData(expense: expense)
        }
        self.setBarItems(closeBool: false)
        addReceiptButton.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadDetails()

    }
    
    func setBarItems(closeBool:Bool) {
        
        if let approvers = expenseModel?.approvers
        {
            if approvers.contains(UserDefaults.standard.userId)
            {
                let btnSave = UIButton(type: .custom)
                btnSave.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                btnSave.setTitleColor(UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0), for: .normal)
                if closeBool
                {
                    btnSave.setTitle("Close", for: .normal)
                    btnSave.addTarget(self, action: #selector(self.cancelBtnAction), for: .touchUpInside)
                }
                else
                {
                    btnSave.setTitle("Approve", for: .normal)
                    btnSave.addTarget(self, action: #selector(self.saveBtnAction), for: .touchUpInside)
                }
                let itemBtnSave = UIBarButtonItem(customView: btnSave)
                self.navigationItem.setRightBarButtonItems([itemBtnSave], animated: true)
            }
            
        }
        else
        {
            self.navigationItem.setRightBarButtonItems([], animated: true)

        }
        
        
       
    }

    
    @objc func saveBtnAction()  {
            self.actions()
    }
    @objc func cancelBtnAction()  {
        remove()
        self.setBarItems(closeBool: false)
    }
    func actions() {
        let vc = ExpenseDetailsPopUpVC.instantiate(fromAppStoryboard: .Main)
        vc.delegate = self
        vc.expense = self.expenseModel
        add(vc)
        self.setBarItems(closeBool: true)

    }
    @IBAction func addCostAction(_ sender: Any) {
        let vc = CostallocationViewController.instantiate(fromAppStoryboard: .Main)
        vc.expense = self.expenseModel
        navigationController?.pushViewController(vc, animated: true)
    }
    func loadDetails()  {
        self.setBarItems(closeBool: false)
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
       // self.showHud()

        let parameters: Parameters = [ "userId": UserDefaults.standard.userId]
        NetworkManager.getExpenseDetails(parameters: parameters, method: .get, expenseID: expenseModel?.id ?? "") { (success, json, message) in
            self.hideHud()
            if success {
                let expense = ExpenseModel(json)
                self.expenseModel = expense
                DispatchQueue.main.async {
                    self.displayData(expense: expense)
                }
                if self.expenseImage == nil
                {
                self.loadImage()
                }
            } else {
                self.alert(message: message)
            }
        }
    }
    func loadImage()
    {
       
        
        let header: HTTPHeaders = ["Authorization": UserDefaults.standard.Authorization]
        Alamofire.request(expenseModel?.imageLocation ?? "", method: .get, parameters: [:], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            DispatchQueue.main.async {
                //self.hideHud()
             //  print (response.data)
                if let data = response.data
                {
                    self.expenseImage = UIImage(data:data)
                    if self.expenseImage != nil
                    {
                        self.imgView.image = self.expenseImage
                        self.imgView.contentMode = .scaleToFill
                        self.imgView.layer.cornerRadius = 5
                    }
                }
                

            }
           
        }
    }
    @IBAction func photoDetail(_ sender: Any) {
          if expenseImage != nil
          {
        let vc = PhotoDetailViewController.instantiate(fromAppStoryboard: .Main)
        vc.expenseImage = expenseImage
        navigationController?.pushViewController(vc, animated: true)
        }
    }
    func displayData(expense:ExpenseModel)  {
        self.expenseModel = expense
        self.reimbursedAmountLabel.text = "\(expense.orderAmountInCents!)"
        employeeLabel.text = expense.employee
        merchantLabel.text = expense.merchantName
        statusLabel.text = expense.status
        typeLabel.text = expense.type
        if expense.receiptName == nil || expense.receiptName?.count == 0
        {
            addReceiptButton.isHidden = false
            receiptLabel.isHidden = true
        }
        else
        {
            
            addReceiptButton.isHidden = true
            receiptLabel.isHidden = false
            receiptLabel.text = expense.receiptName
        }

        if expense.costAllocations?.count == 0 || expense.costAllocations == nil
        {
            addCostButton.isHidden = false
            costAllocationLabel.isHidden = true
        }
        else
        {
            addCostButton.isHidden = true
            costAllocationLabel.isHidden = false
        }
        costAllocationLabel.text = expense.costAllocations
        let amount:Double = Double((expense.orderAmountInCents)!)/100
        self.reimbursedAmountLabel.text = String(format: "%@ %.2f", expense.orderCurrency ?? "GBP",  amount )
        self.expenseAmountLabel.text = String(format: "%@ %.2f", expense.orderCurrency ?? "GBP",  amount )
        self.setBarItems(closeBool: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Actions
   @IBAction  func addReceiptAction() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        let alert = UIAlertController(title: "Yordex", message: "Please select an option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
            }
            self.present(imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            //Photos permission
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        imagePicker.sourceType = .photoLibrary
                        self.present(imagePicker, animated: true, completion: nil)
                    } else {
                        self.alert(message: "Please permit the app to use Photo Library")
                    }
                })
            }
            else if photos == .authorized
            {
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func approveRejectExpenseAction(action:Bool) {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        
        
        let header: HTTPHeaders = ["Authorization": UserDefaults.standard.Authorization]
        
        let encoded = self.expenseModel?.id?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        // self.showHud()
        let parameters = ["approved": "\(action)", "closeOrder" :"false", "userId" : UserDefaults.standard.userId]
        let urlString = String(format:"%@internalapprovals/approvals?orderId=%@",kBaseUrl,"\(encoded!)")
        //print(urlString)
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            DispatchQueue.main.async {
                //       self.hideHud()
            }
            //let json = JSON(response.result.value ?? "")
            //print(json)
            self.loadDetails()
            
        }
    }
    func addReceipt() {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        self.showHud()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date: String = dateFormatter.string(from: Date())
        let image: UIImage = pickedImage["image"] as! UIImage
        let base64: String = image.toBase64() ?? ""
        
        let parameters = ["orderId": self.expenseModel?.id ?? "", "eventId": self.expenseModel?.eventId ?? "", "base64EncodedContent": base64, "fileName": "\(pickedImage["name"]!)", "type": "INVOICE", "customerDocumentId": "Receipt", "documentDate" : date] as [String : Any]
       // print(parameters)
        
        let header: HTTPHeaders = ["Authorization": UserDefaults.standard.Authorization]
        
        Alamofire.request(kUrlAddReceipt, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            self.hideHud()
            self.loadDetails()

          //  let json = JSON(response.result.value ?? "")
      //  print(json)
        }
    }

}
// MARK: - ImagePickerController delegate
extension DetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            pickedImage["image"] = self.resizeImage(image: image, targetSize: CGSize(width: 200.0, height: 200.0))
            
            pickedImage["name"] = "Img001.png"
            if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
               // print(asset.value(forKey: "filename") ?? "")
                let str:String = asset.value(forKey: "filename") as! String
                pickedImage["name"] =  str.replacingOccurrences(of: "HEIC", with: "PNG")
                
            }
            
            self.addReceipt()
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
// MARK: - ExpenseDelegate
extension DetailsViewController: ExpenseDetailsDelegate {
    func selected(index: Int) {
        //addBarButtonTapped(addBarButton ?? UIBarButtonItem())
        remove()
        switch index {
            
        case 0:
            //print("index = 0")
            self.addReceiptAction()
            break
        case 1:
            self.approveRejectExpenseAction(action: true)
            //print("index = 1")
            break
        case 2:
            self.approveRejectExpenseAction(action: false)

            //print("index = 2")
            break
        default:
            print("index = Default")
        }
    }
}
