//
//  ManuallyAddExpenseViewController.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 02/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
import Photos
import SwiftyJSON
import Alamofire

class ScanReceiptController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var merchantTF: UITextField!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var unitTF: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var budgetLineTF: UITextField!
    @IBOutlet weak var addReceiptButton: UIButton!
    @IBOutlet weak var taxesTF: UITextField!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var currencyView: UIView!
    // Error Labels
    @IBOutlet weak var merchantErrorLabel: UILabel!
    @IBOutlet weak var amountErrorLabel: UILabel!
    @IBOutlet weak var budgetErrorLabel: UILabel!
    @IBOutlet weak var receiptErrorLabel: UILabel!
    @IBOutlet weak var TaxesLabel: UILabel!
    // MARK: - Variables
    var pickedImage = [String:Any]()
    var myPickerView = UIPickerView()
    var txtFieldSelected = UITextField()
    var budgetList = [BudgetModel]()
    var currencyCode = ["AFN", "ALL", "DZD", "USD", "EUR", "AOA", "XCD", "ARS", "AMD", "AWG", "AUD", "EUR", "AZN", "BSD", "BHD", "BDT", "BBD", "BYN", "EUR", "BZD", "XOF", "BMD", "BTN", "INR", "BOB", "BOV", "USD", "BAM", "BWP", "NOK"]
    var datePicker = UIDatePicker()
    var budgetId: String?
    var eventId: String = ""
    var orderId: String = ""
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        self.title = "Scan Receipt"
        super.viewDidLoad()
        configureView()
        setBarItems()
       // setDatePicker()
        
    }
    func configureView() {
        budgetLineTF.setUpBorder(width: 1, color: grayBorder, radius: 8)
    }
    
    func setDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateTF.text = dateFormatter.string(from: Date())
        
        datePicker.datePickerMode = .date
        dateTF.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateTF.text = dateFormatter.string(from: sender.date)
    }
    
    func setBarItems() {
        let btnCancel = UIButton(type: .custom)
        btnCancel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0), for: .normal)
        btnCancel.addTarget(self, action: #selector(self.cancelBtnAction), for: .touchUpInside)
        let itemBtnCancel = UIBarButtonItem(customView: btnCancel)
        self.navigationItem.setLeftBarButtonItems([itemBtnCancel], animated: true)
        
        let btnSave = UIButton(type: .custom)
        btnSave.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnSave.setTitle("Save", for: .normal)
        btnSave.setTitleColor(UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0), for: .normal)
        btnSave.addTarget(self, action: #selector(self.saveBtnAction), for: .touchUpInside)
        let itemBtnSave = UIBarButtonItem(customView: btnSave)
        self.navigationItem.setRightBarButtonItems([itemBtnSave], animated: true)
        
    }
    
    @objc func saveBtnAction()  {
        if validate() {
            self.createExpense()
        }
    }
    
    @objc func cancelBtnAction()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validate() -> Bool {
        var isValid = true
        
     
        if budgetLineTF.text?.isEmpty ?? false {
            isValid = false
            budgetErrorLabel.isHidden = false
        }
        if pickedImage.isEmpty {
            isValid = false
            receiptErrorLabel.isHidden = false
        }
        
        return isValid
    }
    
    // MARK: - Actions
    @IBAction func addReceiptTapped(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        let alert = UIAlertController(title: "Yordex", message: "Please select an option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
            }
            self.present(imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            //Photos permission
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        imagePicker.sourceType = .photoLibrary
                        self.present(imagePicker, animated: true, completion: nil)
                    } else {
                        self.alert(message: "Please permit the app to use Photo Library")
                    }
                })
            }
            else if photos == .authorized
            {
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - ImagePickerController delegate
extension ScanReceiptController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            pickedImage["image"] = self.resizeImage(image: image, targetSize: CGSize(width: 200.0, height: 200.0))
            
            pickedImage["name"] = "Img001.png"
            if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
                print(asset.value(forKey: "filename") ?? "")
                let str:String = asset.value(forKey: "filename") as! String
                pickedImage["name"] =  str.replacingOccurrences(of: "HEIC", with: "PNG")
                
            }
            addReceiptButton.setTitle(pickedImage["name"] as? String, for: .normal)
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

// Mark: - UITextViewDelegate
extension ScanReceiptController : UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        if textField == budgetLineTF {
            if !Connectivity.isConnectedToInternet() {
                textField.resignFirstResponder()
                return
            }
            self.txtFieldSelected = budgetLineTF
            self.getBudgetLineDetails()
            self.pickUp(self.txtFieldSelected)
        }
        else if textField == amountTF
        {
            self.txtFieldSelected = amountTF
            self.pickUp(self.txtFieldSelected)
        }
        
    }
    
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtFieldSelected == self.budgetLineTF{
            return  budgetList.count
        }else{
            return  currencyCode.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var name = String()
        if txtFieldSelected == self.budgetLineTF{
            name = (budgetList[row].value!)
        }else{
            name = currencyCode[row]
        }
        return name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtFieldSelected == self.budgetLineTF{
            self.txtFieldSelected.text = budgetList[row].value!
            self.budgetId = budgetList[row].id!
        }else{
            self.txtFieldSelected.text = currencyCode[row]
            // self.selectedRegion = arrRegions[row]
        }
        
    }
    
    @objc func doneClick() {
        
        if self.txtFieldSelected == self.amountTF {
            
            if self.amountTF.text?.count ?? 0 == 0
            {
                if currencyCode.count > 0
                {
                    self.amountTF.text = currencyCode[0]
                }
            }
            
            self.TaxesLabel.text = String(format: "Taxes (%@)", self.amountTF!.text ?? "")
        }
        if self.txtFieldSelected == self.budgetLineTF {
            
            if self.budgetLineTF.text?.count ?? 0 == 0
            {
                if budgetList.count > 0
                {
                    self.budgetLineTF.text = budgetList[0].value!
                    self.budgetId = budgetList[0].id!
                }
            }
            
        }
        
        self.txtFieldSelected.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        self.txtFieldSelected.resignFirstResponder()
    }
}

// Mark: - Web Services
extension ScanReceiptController {
    
    func getBudgetLineDetails() {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        //  DispatchQueue.main.async {
        self.showHud()
        let parameters: Parameters = ["name" : "budgetLine", "page" : "0", "size" : "5000"]
        NetworkManager.getBudgetList(parameters: parameters, method: .get) { (success, json, message) in
            self.hideHud()
            if success {
                if let _embedded = json["_embedded"].dictionary, let orders = _embedded["budgetList"]?.array {
                    orders.forEach({ (data) in
                        let budget = BudgetModel(data)
                        self.budgetList.append(budget)
                    })
                    DispatchQueue.main.async {
                        self.myPickerView.reloadAllComponents()
                    }
                }
            }else {
                self.alert(message: message)
            }
        }
    }
    
    
    func createExpense() {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        DispatchQueue.main.async {
            
            self.showHud()
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date: String = dateFormatter.string(from: Date())
        let image: UIImage = pickedImage["image"] as! UIImage
        let base64: String = image.toBase64() ?? ""
        
        let buyerTag = ["orderType" : "EXPENSE"]
        
        let budget = ["budgetLine" : self.budgetId]
        let buyerCostAllocation = ["budget" : budget, "allocation" : "100"] as [String : Any]
        
        // let line = ["type" : "PRODUCT", "name" : "Expense", , "vatAmountInCents" : "\(vatAmount)", "quantity" : "1", "listPriceInCents" : "\(priceInCents)"] as [String : Any]
        
        let documents = ["base64EncodedContent" : base64,"fileName" : "\(pickedImage["name"]!)", "name" : "\(pickedImage["name"]!)", "type" : "INVOICE", "customerDocumentId" : "Receipt", "documentDate" : date] as [String : Any]
        
        let events = ["eventName" : "Cash or card payment by employee", "paymentType" : "PAYOUT", "pctToBePaid" : "100", "documents" : [documents]  , "eventNumber" : "1"] as [String : Any]
        let event2 = ["eventName" : "Expense reimbursement", "paymentType" : "PAYIN", "pctToBePaid" : "100", "eventNumber" : "2"] as [String : Any]

        
        let parameters = ["buyerId": UserDefaults.standard.traderId, "sellerId": "EXPENSES", "buyerUserId": UserDefaults.standard.userId, "sellerOrderReference": "PROCESSING", "vatIncluded": "false", "buyerTags": buyerTag, "events": [events,event2] , "buyerCostAllocation" : [buyerCostAllocation]] as [String : Any]
        print(parameters)
        
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: parameters,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {
            print("JSON string = \n\(theJSONText)")
        }
        
        let header: HTTPHeaders = ["Authorization": UserDefaults.standard.Authorization]
        
        Alamofire.request(kUrlCreateExpense, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            DispatchQueue.main.async {
                //self.hideHud()
                
            }
            let json = JSON(response.result.value ?? "")
            
            print(json)
            
            
            if json["id"].stringValue != "" {
                if let event = json["events"].array
                {
                    if event.count > 0
                    {
                        self.eventId = event[0]["id"].stringValue
                    }
                }
                
                self.createPaymentRecord()
            } else {
                self.hideHud()
                if json["errors"].arrayValue.count > 0 {
                    let errorDictionary = json["errors"].arrayValue[0]
                    let message = errorDictionary["message"].stringValue
                    self.alert(message: message)
                }
            }
        }
    }
    func createPaymentRecord() {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        let header: HTTPHeaders = ["Authorization": UserDefaults.standard.Authorization]
        DispatchQueue.main.async {
            
            //self.showHud()
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let image: UIImage = pickedImage["image"] as! UIImage
        let base64: String = image.toBase64() ?? ""
        
        let parameters = ["email":[UserDefaults.standard.userId],"eventId": self.eventId, "type": "INVOICE", "fileName":"\(pickedImage["name"]!)" , "base64EncodedContent": base64 ] as [String : Any]
        Alamofire.request(kTraderOrder, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            DispatchQueue.main.async {
                self.hideHud()
                
                self.resetFields()
                let alert = UIAlertController(title: "Success", message: "Receipt uploaded successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                    }}))
                
                self.present(alert, animated: true, completion: nil)
            }
            
            let json = JSON(response.result.value ?? "")
            print(json)
            // self.viewWillAppear(true)
            //self.approveExpenseForCreatedExpense()
        }
    }
    
    
    func resetFields()  {
        self.budgetLineTF.text = ""
        self.addReceiptButton.setTitle("+ Add Receipt", for: .normal)
    }
    
   
    
}

