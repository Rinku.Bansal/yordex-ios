//
//  ExpenseDetailsPopUpVC.swift
//  Yordex
//
//  Created by vinove on 16/09/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
protocol ExpenseDetailsDelegate {
    func selected(index: Int)
}
class ExpenseDetailsPopUpVC: UIViewController {
    @IBOutlet weak var addReceiptButton: UIButton!
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    var delegate: ExpenseDetailsDelegate!
    var expense:ExpenseModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        if expense?.receiptName != nil
        {
            self.addReceiptButton.isEnabled = false
            self.addReceiptButton.alpha = 0.5
        }
        if expense?.status == "APPROVED"
        {
           self.hideApprove()
        }
        else
        {
            

            let userId =  UserDefaults.standard.userId
            if let approvers = expense?.approvers, let approvals = expense?.approvals
            {
            if approvers.contains(userId)
            {
                if approvals.contains(userId)
                {
                  self.hideApprove()
                }
                
            }
            else
            {
                self.hideApprove()
            }
            }
            
            
            
        }
        
        // Do any additional setup after loading the view.
    }
    func hideApprove()
    {
        self.approveButton.isEnabled = false
        self.approveButton.alpha = 0.5
        self.rejectButton.isEnabled = false
        self.rejectButton.alpha = 0.5
    }
    
    @IBAction func addReceiptAction(_ sender: Any) {
        self.delegate.selected(index: 0)

    }
    
    @IBAction func approveExpense(_ sender: Any) {
        self.delegate.selected(index: 1)

    }
    @IBAction func rejectCloseExpense(_ sender: Any) {
        self.delegate.selected(index: 2)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
