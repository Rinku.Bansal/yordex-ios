//
//  ExpensePopUpViewController.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 01/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit

protocol ExpenseDelegate {
    func selected(index: Int)
}

class ExpensePopUpViewController: UIViewController {

    var expenseDelegate: ExpenseDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func manuallyBtnTapped(_ sender: Any) {
        expenseDelegate.selected(index: 0)
    }
    
    @IBAction func receiptTapped(_ sender: Any) {
        expenseDelegate.selected(index: 1)
    }
}
