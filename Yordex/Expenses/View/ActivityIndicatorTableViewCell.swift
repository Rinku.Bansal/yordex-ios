//
//  ActivityIndicatorTableViewCell.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 08/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit

class ActivityIndicatorTableViewCell: UITableViewCell {

    @IBOutlet weak var hud: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
