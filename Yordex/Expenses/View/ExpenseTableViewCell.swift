//
//  ExpenseTableViewCell.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 01/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    var expense: ExpenseModel? {
        didSet {
            setValues()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
    func setValues() {
        companyLabel.text = expense?.merchantName
        statusButton.setTitle(expense?.status, for: .normal)
        let amount:Double = Double((expense?.orderAmountInCents)!)/100
        amountLabel.text = String(format: "%@ %.2f", expense?.orderCurrency ?? "GBP",  amount )
     
    }
}
