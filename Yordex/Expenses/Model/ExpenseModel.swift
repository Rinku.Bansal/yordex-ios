//
//  ExpenseModel.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import SwiftyJSON

class ExpenseModel {
    var orderAmountInCents: Int?
    var orderCurrency: String?
    var status: String?
    var company: String?
    var merchantName: String?
    var id: String?
    var eventId: String?

    var lineId: String?

    var employee:String?
    var type:String?
    var costAllocations:String?
    var receiptName:String?
    var approvers = [String]()
    var approvals = [String]()
    var imageLocation  = ""


    
    init(_ json: JSON) {
        id = json["id"].stringValue
        orderAmountInCents = json["orderAmountInCents"].intValue
        orderCurrency = json["orderCurrency"].stringValue
        merchantName = json["sellerOrderReference"].stringValue
        status = json["status"].stringValue
        company = json["buyerCompanyTradingName"].stringValue
        
        employee = json["buyerUserEmail"].stringValue
        if let type = json["buyerTags"]["orderType"].string {
            self.type = type
        }
        
        
        //Cost Allocation parsing
        if let cost = json["buyerCostAllocation"].array
            {
                if cost.count > 0
                {
                    self.costAllocations = cost[0]["budgetName"].stringValue
                }
            }
        
        //Cost Allocation parsing
        if let lineItem = json["lineItems"].array
        {
            if lineItem.count > 0
            {
                self.lineId = lineItem[0]["id"].stringValue
            }
        }
        
        if let event = json["events"].array
        {
            if let document = event[0]["documents"].array
            {
                
                if document.count > 0
                {
                    self.receiptName = document[0]["fileName"].stringValue
                    self.imageLocation = document[0]["location"].stringValue
                }
            }
          
                self.eventId = event[0]["id"].stringValue
            
        }
        
        if let approvers = json["internalApproval"]["approvers"].array {
            
            for approver  in approvers
            {
                self.approvers.append(approver["userId"].stringValue)
            }
            
        }
        if let approvals = json["internalApproval"]["approvals"].array {
            
            for approval  in approvals
            {
                self.approvals.append(approval["approverUserId"].stringValue)
            }
            
        }
        
        
        
    }
}

