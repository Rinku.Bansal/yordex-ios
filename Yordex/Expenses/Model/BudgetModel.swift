//
//  BudgetModel.swift
//  Yordex
//
//  Created by Rinku vinove on 23/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
import SwiftyJSON

class BudgetModel: NSObject {

    var value: String?
    var id: String?
    
    
    init(_ json: JSON) {
        value = json["value"].stringValue
        id = json["id"].stringValue
    }
    
}
