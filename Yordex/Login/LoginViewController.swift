//
//  LoginViewController.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 01/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTF: YTextField!
    @IBOutlet weak var passwordTF: YTextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var emailErrorLabel: UILabel!
    
    @IBOutlet weak var passwordErrorLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    
    func configureView() {
        emailTF.setAttributes()
        passwordTF.setAttributes()
        loginButton.layer.cornerRadius = 5
        addRightViews()
        emailTF.rightView?.isHidden = true
    }
    func addRightViews() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 12.5, width: 40, height: 25))
        imageView.image = UIImage(named: "error")
        imageView.tintColor = UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        imageView.contentMode = .left
        emailTF.rightView = imageView
        emailTF.rightViewMode = .always
        
        let button = UIButton(frame: CGRect(x: 0, y: 12.5, width: 90, height: 25))
        button.setTitle("Show", for: .normal)
        button.setTitle("Hide", for: .selected)
        var image = UIImage(named: "show_password")
        button.setImage(image, for: .normal)
        image = UIImage(named: "hide_password")
        button.setImage(image, for: .selected)
        button.tintColor = UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        button.setTitleColor(UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0), for: .normal)
        button.setTitleColor(UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 242.0/255.0, alpha: 1.0), for: .selected)
        button.addTarget(self, action: #selector(showPasswordTapped(_:)), for: .touchUpInside)
        passwordTF.rightView = button
        passwordTF.rightViewMode = .always
    }
    // MARK: - Actions
    @IBAction func loginTapped(_ sender: Any) {
        if validate() {
            login()
        }
    }
    @objc func showPasswordTapped(_ sender: UIButton) {
        passwordTF.isSecureTextEntry = sender.isSelected
        sender.isSelected = !sender.isSelected
    }
    
    // MARK: - Methods
    func moveToHomeScreen() {
        let nvc = HomeNavigationController.instantiate(fromAppStoryboard: .Main)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nvc
    }
    
    func validate() -> Bool {
        var isValid = true
        let passwordError = "You didn’t enter a valid password"
        
        if emailTF.text?.isEmpty ?? false {
            isValid = false
            emailErrorLabel.isHidden = false
            emailErrorLabel.text = "Please enter email address"
        }else if !isValidEmail(testStr: emailTF.text!) {
            isValid = false
            emailErrorLabel.isHidden = false
            emailErrorLabel.text = "You didn't enter a valid email address"
        }else {
            emailErrorLabel.isHidden = true
        }
        if passwordTF.text?.isEmpty ?? false {
            isValid = false
            passwordErrorLabel.isHidden = false
            passwordErrorLabel.text = "Please enter password"
        }else if passwordTF.text?.rangeOfCharacter(from: CharacterSet.uppercaseLetters) == nil {
            isValid = false
            passwordErrorLabel.isHidden = false
            passwordErrorLabel.text = passwordError
        }else if passwordTF.text?.rangeOfCharacter(from: CharacterSet.lowercaseLetters) == nil {
            isValid = false
            passwordErrorLabel.isHidden = false
            passwordErrorLabel.text = passwordError
        }else if passwordTF.text?.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil {
            isValid = false
            passwordErrorLabel.isHidden = false
            passwordErrorLabel.text = passwordError
        }else if passwordTF.text?.count ?? 0 < 6 {
            isValid = false
            passwordErrorLabel.isHidden = false
            passwordErrorLabel.text = passwordError
        }else {
            passwordErrorLabel.isHidden = true
        }
        return isValid
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}
// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
}
// MARK: - Web Services
extension LoginViewController {
    func login() {
        if !Connectivity.isConnectedToInternet() {
            self.alert(message: internetNTAV)
            return
        }
        self.showHud()
        let parameters = ["username": emailTF.text!, "password": passwordTF.text!]
        Alamofire.request("https://api.yordex.com/sessions", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response) in
            self.hideHud()
            let json = JSON(response.result.value ?? "")
            print(json)
            if json["token"].stringValue != "" {
                UserDefaults.standard.Authorization = json["token"].stringValue
                UserDefaults.standard.userId = json["userId"].stringValue
                UserDefaults.standard.traderId = json["traderId"].stringValue
                self.moveToHomeScreen()
            } else {
                if json["errors"].arrayValue.count > 0 {
                    let errorDictionary = json["errors"].arrayValue[0]
                    let message = errorDictionary["message"].stringValue
                    self.alert(message: message)
                }
            }
        }
    }
}
