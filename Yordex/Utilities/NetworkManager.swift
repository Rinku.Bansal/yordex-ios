//
//  NetworkManager.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager {
    // MARK: - Manually Add Expense
    // MARK: - Home
    
    class func getExpenseList(parameters: Parameters, method: HTTPMethod, page: Int, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        
        let encoded = UserDefaults.standard.userId.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        let url = kBaseUrl + kUrlExpenseList + "?page=" + "\(page)"
        + "&buyerTagValues=EXPENSE&returnCompletedOrders=false&userId=" + "\(encoded!)" + "&size=25"
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": UserDefaults.standard.Authorization]).responseJSON { (response) in
            let json = JSON(response.result.value ?? "")
           // print(json)
            
            if json["errors"].arrayValue.count > 0 {
                let errorDictionary = json["errors"].arrayValue[0]
                let message = errorDictionary["message"].stringValue
                completion(false, JSON.init(), message)
            } else {
                completion(true, json, "")
            }
        }
    }
    
    class func getExpenseDetails(parameters: Parameters, method: HTTPMethod, expenseID: String, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        
        
        let encoded = expenseID.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        let url = kBaseUrl + kUrlExpenseDetail + "/" + "\(encoded!)"
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": UserDefaults.standard.Authorization]).responseJSON { (response) in
            let json = JSON(response.result.value ?? "")
           // print(json)//
            
            if json["errors"].arrayValue.count > 0 {
                let errorDictionary = json["errors"].arrayValue[0]
                let message = errorDictionary["message"].stringValue
                completion(false, JSON.init(), message)
            } else {
                completion(true, json, "")
            }
        }
    }
    
    class func getBudgetList(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        let url = kBaseUrl + kUrlBudgetList + "name=" + "budgetLine" + "&page=" + "0" + "&size=" + "5000"
       // print(url)
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": UserDefaults.standard.Authorization]).responseJSON { (response) in
            let json = JSON(response.result.value ?? "")
           // print(json)
            if json["errors"].arrayValue.count > 0 {
                let errorDictionary = json["errors"].arrayValue[0]
                let message = errorDictionary["message"].stringValue
                completion(false, JSON.init(), message)
            } else {
                completion(true, json, "")
            }
        }
    }
    
    class func logout(parameters: Parameters, method: HTTPMethod, completion: @escaping ((_ success: Bool, _ json: JSON, _ message: String) -> Void)) {
        let url = kBaseUrl + kUrlLogout
        //print(url)

        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization": UserDefaults.standard.Authorization]).responseJSON { (response) in
            let json = JSON(response.result.value ?? "")
           // print(json)
            if json["errors"].arrayValue.count > 0 {
                let errorDictionary = json["errors"].arrayValue[0]
                let message = errorDictionary["message"].stringValue
                completion(false, JSON.init(), message)
            } else {
                completion(true, json, "")
            }
        }
    }
    
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

// MARK: - App URLs
let kBaseUrl = "https://api.yordex.com/"
let kUrlExpenseList = "orders"
let kUrlExpenseDetail = "aggregate/orders"
let kUrlCostAllocation = kBaseUrl + "orders/"


let kUrlBudgetList = "budgets?"
let kUrlLogout = "sessions"


let kUrlAddComment = kBaseUrl + "timelines/comments?"
let kUrlCreateExpense = kBaseUrl + "aggregate/orders?"
let kUrlAddReceipt = kBaseUrl + "documents"

let kUrlApproveExpense = kBaseUrl + "​internalapprovals/approvals?"
let kUrlPayment = kBaseUrl + "paymentrecords"
let kTraderOrder = kBaseUrl + "traderorders"


// MARK: - Internet Not availabel
let internetNTAV = "Internet not available"

