//
//  Table.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 06/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation

/*
func setUpTableView() {
    expenseTableView.rowHeight = UITableView.automaticDimension
    expenseTableView.estimatedRowHeight = 50
    expenseTableView.register(UINib(nibName: TableViewCells.BudgetTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.BudgetTableViewCell.rawValue)
    expenseTableView.register(UINib(nibName: TableViewCells.ExpenseBasicsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.ExpenseBasicsTableViewCell.rawValue)
    expenseTableView.register(UINib(nibName: TableViewCells.AttachmentsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.AttachmentsTableViewCell.rawValue)
    expenseTableView.register(UINib(nibName: TableViewCells.TaxesTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: TableViewCells.TaxesTableViewCell.rawValue)
}

// MARK: - TableView methods
extension ManuallyAddExpenseViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.ExpenseBasicsTableViewCell.rawValue, for: indexPath) as! ExpenseBasicsTableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.BudgetTableViewCell.rawValue, for: indexPath) as! BudgetTableViewCell
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.AttachmentsTableViewCell.rawValue, for: indexPath) as! AttachmentsTableViewCell
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.TaxesTableViewCell.rawValue, for: indexPath) as! TaxesTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCells.AttachmentsTableViewCell.rawValue, for: indexPath) as! AttachmentsTableViewCell
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
}
*/
