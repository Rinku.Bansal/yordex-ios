//
//  AppColors.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 06/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import UIKit

let grayBorder = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0)
let barItemColor = UIColor(red: 24.0, green: 170.0, blue: 242.0, alpha: 1.0)
