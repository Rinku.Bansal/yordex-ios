//
//  UIViewControllerExt.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

fileprivate var hudKey: UInt8 = 0

extension UIViewController {
    func alert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Yordex", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func remove() {
        self.children.forEach {
            $0.didMove(toParent: nil)
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        }
    }
    
    // 
    fileprivate var hud: MBProgressHUD?{
        get{
            return objc_getAssociatedObject(self, &hudKey) as? MBProgressHUD
        }
        set{
            objc_setAssociatedObject(self, &hudKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            //            newValue?.contentColor = Appearance.greenColor
            newValue?.bezelView.style = .solidColor
            //            newValue?.bezelView.backgroundColor = ThemeManager.isNightMode() ? Appearance.backgroundViewNight : .white
            newValue?.backgroundColor = .clear
        }
    }
    func showHud(_ message: String = ""){
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideHud(){
        DispatchQueue.main.async {
            self.hud?.hide(animated: true)
        }
    }
}
