//
//  Extensions.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 02/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    private static var _indexPath = [String:IndexPath]()
    var indexPath: IndexPath {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UITableViewCell._indexPath[tmpAddress] ?? IndexPath(row: 0, section: 0)
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UITableViewCell._indexPath[tmpAddress] = newValue
        }
    }
}
extension UICollectionViewCell {
    private static var _indexPath = [String:IndexPath]()
    var indexPath: IndexPath {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UICollectionViewCell._indexPath[tmpAddress] ?? IndexPath(row: 0, section: 0)
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UICollectionViewCell._indexPath[tmpAddress] = newValue
        }
    }
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}

