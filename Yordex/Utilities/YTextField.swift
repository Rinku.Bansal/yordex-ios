//
//  YTextField.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 01/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation
import UIKit

class YTextField: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0))
    }
    func setAttributes() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 5
    }
}
