//
//  UserDefaultsExt.swift
//  Yordex
//
//  Created by Abhinav Dobhal on 07/08/19.
//  Copyright © 2019 Yordex. All rights reserved.
//

import Foundation

extension UserDefaults {
    var Authorization: String {
        get {
            return UserDefaults.standard.object(forKey: "token0") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "token0")
            defaults.synchronize()
        }
    }
    var userId: String {
        get {
            return UserDefaults.standard.object(forKey: "userId") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "userId")
        }
    }
    var traderId: String {
        get {
            return UserDefaults.standard.object(forKey: "traderId") as? String ?? ""
        }
        set(newValue) {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "traderId")
        }
    }
    
}
